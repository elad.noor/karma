# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import itertools
import json
import math
from typing import Dict, List, Set, Tuple

import numpy as np
import pandas as pd
import pydot
import seaborn as sns
from binarytree import Node
from matplotlib import pyplot as plt
from numpy import random
from scipy.linalg import expm


class AssemblyTree(object):
    def __init__(
        self,
        root: Node,
        preys: List[str],
        baits: List[str],
        poolsizes: np.array,
        params: dict,
    ):
        """A class for fitting tree poolsizes to data (phi values).

        :param root: A Node object of the root of the tree
        :param preys: A list of prey names
        :param baits: A list of bait names, must be a subset of the preys
        :param poolsizes: An array of size twice the number of preys,
        containing the poolsize of each free protein and each complex
        in the binary assembly tree. We always normalize the poolsizes
        so that the largest one is equal to 1.
        """
        self.root = root
        self.preys = preys
        self.baits = baits
        self.poolsizes = poolsizes
        self.params = params

        assert self.root.leaf_count == len(
            preys
        ), "Leaf count does not match the number of preys"

        assert poolsizes.shape == (2 * len(preys) - 1,)

        self.poolsizes /= max(self.poolsizes)
        self.ref_phis = None
        self.ref_weights = None
        self.phis = None
        self.update_phis()

    @staticmethod
    def _set_reference_phis(
        ref_phi_df: pd.DataFrame, preys: List[str], baits: List[str]
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Store the precursor pool data as two matrices.

        Args:
            ref_phi_df: a DataFrame containing all the experimental data

        """
        # first, copy the phi data into a temporary array, so they
        # can be normalized (depending on the selection option)

        ref_phis = np.zeros((len(baits), len(preys)))
        ref_weights = np.zeros((len(baits), len(preys)))
        _df = ref_phi_df.set_index(["bait", "prey"])
        for i, bait in enumerate(baits):
            for j, prey in enumerate(preys):
                if bait == prey:
                    ref_phis[i, j] = 1.0
                    ref_weights[i, j] = 0.0
                else:
                    phi = _df.at[(bait, prey), "phi1"]
                    ref_phis[i, j] = phi.nominal_value
                    ref_weights[i, j] = 1.0 / phi.std_dev
        return ref_phis, ref_weights

    def set_reference_phis(self, ref_phi_df: pd.DataFrame) -> None:
        self.ref_phis, self.ref_weights = self._set_reference_phis(
            ref_phi_df, self.preys, self.baits
        )

    def get_reference_phis(self) -> Tuple[np.array, np.array]:
        if self.ref_phis is None:
            raise ValueError(
                "Must set the reference correlation matrix "
                "before calculating scores."
            )
        return self.ref_phis, self.ref_weights

    @property
    def n_leaves(self) -> int:
        return self.root.leaf_count

    @property
    def n_nodes(self) -> int:
        return self.poolsizes.shape[0]

    @property
    def lysine_psi(self) -> float:
        if "lysine_psi" not in self.params:
            raise KeyError("The Lysine pool Ψ parameter has not been set")
        return self.params["lysine_psi"]

    def calc_total_pool_sizes(self) -> dict:
        """Calculate the total pool sizes (per prey)

        Returns:
            res: a dictionary mapping preys to total pool sizes

        """
        return self.calc_total_pool_sizes_recursive(self.root)

    def calc_total_pool_sizes_recursive(self, node) -> dict:
        """Calculate all total pool sizes.

        recursively calculate the total pool sizes for each gene (prey),
        by summing all complexes on the branch of the tree leading to it.

        Args:
            node: a node representing the subtree (or the root)

        Returns:
            res: a dictionary mapping preys to total pool sizes
        """
        if node.height == 0:
            # if the current node is a leaf, we initialize the dictionary with
            # the pool size of this leaf
            prey = self.preys[node.value]
            return {prey: self.poolsizes[node.value]}

        # recursion step: calculate the total pool sizes for each
        # of the two child subtrees
        res_left = self.calc_total_pool_sizes_recursive(node.left)
        res_right = self.calc_total_pool_sizes_recursive(node.right)

        # since the preys of the two subtrees are mutually exclusive,
        # sets, we can just merge the two dictionaries
        res = res_left | res_right

        # add this node to all the pools in the dictionary
        for key in res.keys():
            res[key] += self.poolsizes[node.value]
        return res

    def calc_precursor_pool_sizes(self, bait: str) -> dict:
        """Calculate all precursor pool sizes wrt 'bait'.

        Args:
            bait: the bait relative to which we calculate the pool sizes

        Returns:
            res: a dictionary mapping preys to precursor pool sizes
        """
        res, _ = self.calc_precursor_pool_sizes_recursive(self.root, bait)
        return res

    def calc_precursor_pool_sizes_recursive(
        self, node: Node, bait: str
    ) -> Tuple[Dict[str, float], bool]:
        """Run a DFS on tree to calculate all precursor pool sizes wrt 'bait'.

        Args:
            node: a node representing the subtree (or the root)
            bait: the bait relative to which we calculate the precursor pool
            sizes

        Returns:
            res: a dictionary mapping preys to precursor pool sizes
            stop: a boolean flag indicating whether the bait is part of this
            subtree or not
        """
        if node.height == 0:
            # if the current node is a leaf
            prey = self.preys[node.value]
            if prey == bait:
                # if leaf is the bait itself, there is nothing to do and also
                # tell the calling function to ignore this subtree (stop = True)
                return {}, True
            else:
                # otherwise, we initialize the dictionary with the pool size
                # of this leaf
                return {prey: self.poolsizes[node.value]}, False

        # recursion step: calculate the precursor pool sizes for each
        # of the two child subtrees
        res_left, stop_left = self.calc_precursor_pool_sizes_recursive(
            node.left, bait
        )
        res_right, stop_right = self.calc_precursor_pool_sizes_recursive(
            node.right, bait
        )

        # since the preys of the two subtrees are mutually exclusive,
        # we can just merge the two dictionaries
        res = res_left | res_right

        # if one of the subtrees contains the bait as a leaf, don't add the
        # current node's poolsize (since it is not part of the precursor pool)
        stop = stop_left or stop_right

        # otherwise, add this node to all the pools in the dictionary,
        # since it is still a precursor
        if not stop:
            for key in res.keys():
                res[key] += self.poolsizes[node.value]

        return res, stop

    def update_phis(self):
        """
        for each bait/prey pair, we calculate the precursor pool size
        (all the sub-complexes before they meet,
        divided by all the sub-complexes where the prey appears).
        """
        # calculate the s1 + s2 pools
        s1_plus_s2_dict = self.calc_total_pool_sizes()

        # The default value (e.g. when bait=prey) is 1, so that we will have
        # a zero in the log transformed values. This will anyhow be ignored
        # in the scoring.
        self.phis = np.ones((len(self.baits), len(self.preys)))
        for i, bait in enumerate(self.baits):
            # calculate the s1 pools
            s1_dict = self.calc_precursor_pool_sizes(bait)
            for j, prey in enumerate(self.preys):
                if prey != bait:
                    self.phis[i, j] = s1_dict[prey] / s1_plus_s2_dict[prey]

    def get_score(self) -> float:
        """Get the MSE-based score."""
        ref_phis, ref_weights = self.get_reference_phis()
        return np.mean(
            (self.phis.flatten() - ref_phis.flatten()) ** 2
            * ref_weights.flatten()
        ) / np.mean(ref_weights.flatten())

    @staticmethod
    def random(
        preys: List[str],
        baits: List[str],
        params: dict,
    ):
        n_leaves = len(preys)
        encoding = AssemblyTree.generate_random_encoding(n_leaves)
        root = AssemblyTree.decode(encoding)
        if baits is None:
            baits = preys
        else:
            assert set(baits).issubset(preys)
        poolsizes = random.rand(2 * n_leaves - 1)
        return AssemblyTree(root, preys, baits, poolsizes, params=params)

    def clone(self):
        t = AssemblyTree(
            self.root, self.preys, self.baits, self.poolsizes, self.params
        )
        t.ref_phis = self.ref_phis
        t.ref_weights = self.ref_weights
        return t

    def update_and_get_score(self, poolsizes: np.array):
        self.poolsizes = poolsizes
        self.update_phis()
        return self.get_score()

    def plot_tree_recursive(self, ax: plt.Axes, node: Node, x: float, y: float):
        poolsize = self.poolsizes[node.value]
        if node.height == 0:  # is a leaf
            prey = self.preys[node.value]
            ax.text(
                x,
                y,
                f"{prey}: {poolsize:.4f}",
                va="bottom",
                ha="center",
                rotation=90,
                bbox=dict(facecolor="white", alpha=1),
            )
        else:
            # always draw the larger branch on the right side
            if node.left.leaf_count > node.right.leaf_count:
                larger_branch = node.left
                smaller_branch = node.right
            else:
                larger_branch = node.right
                smaller_branch = node.left

            x_smaller = x - smaller_branch.leaf_count * 5
            x_larger = x + larger_branch.leaf_count * 5
            self.plot_tree_recursive(ax, smaller_branch, x_smaller, y + 5)
            self.plot_tree_recursive(ax, larger_branch, x_larger, y + 5)
            ax.text(
                x,
                y,
                f"{poolsize:.4f}",
                va="top",
                ha="center",
                rotation=0,
                bbox=dict(facecolor="white", alpha=1),
            )
            ax.plot([x, x_smaller], [y, y + 5], "-k")
            ax.plot([x, x_larger], [y, y + 5], "-k")

    def plot_tree(self, ax: plt.Axes):
        self.plot_tree_recursive(ax, self.root, 0, 5)
        ax.set_ylim(0, self.root.max_leaf_depth * 5 + 20)
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.spines["bottom"].set_visible(False)
        ax.spines["left"].set_visible(False)
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])

    def plot_stats(self, vmin=0.0, vmax=1.0) -> plt.Figure:
        phi_label = r"$\phi_1$"
        fig = plt.figure(figsize=(12, 12))
        curr_phis = pd.DataFrame(
            data=self.phis, index=self.baits, columns=self.preys
        )

        ref_phis, ref_weights = self.get_reference_phis()
        ref_phi_df = pd.DataFrame(
            data=ref_phis, index=self.baits, columns=self.preys
        )

        mask = np.zeros((len(self.baits), len(self.preys)))
        for i, bait in enumerate(self.baits):
            for j, prey in enumerate(self.preys):
                if bait == prey:
                    mask[i, j] = 1

        # compare between the raw phi data
        ax = fig.add_subplot(2, 2, 1)
        ax.set_title(f"KSM fitted {phi_label}")
        sns.heatmap(
            ref_phi_df,
            ax=ax,
            cmap="viridis",
            vmin=vmin,
            vmax=vmax,
            annot=True,
            mask=mask,
        )
        ax.set_ylim(0, ref_phi_df.shape[0])
        ax = fig.add_subplot(2, 2, 2)
        ax.set_title(f"Tree generated {phi_label}")
        sns.heatmap(
            curr_phis,
            ax=ax,
            cmap="viridis",
            vmin=vmin,
            vmax=vmax,
            annot=True,
            mask=mask,
        )
        ax.set_ylim(0, curr_phis.shape[0])

        colormap = {
            "NUP85": "#bd42ff",
            "NUP84": "#ef0052",
            "NUP145C": "#ef0052",
            "NUP133": "#ef0052",
            "*NUP53": "#ff9400",
            "NUP82": "#5aa518",
            "NUP159": "#5aa518",
            "NUP57": "#5aa5c6",
            "NUP49": "#5aa5c6",
            "NUP188": "#4200ff",
            "MLP1": "#bdbdbd",
            "*MLP2": "#ff8080",
            "NUP1": "#a99480",
        }

        ax = fig.add_subplot(2, 2, 3)
        for j, prey in enumerate(self.preys):
            idx = np.nonzero(ref_weights[:, j])[0]
            ax.errorbar(
                x=ref_phis[idx, j],
                y=self.phis[idx, j],
                xerr=1.0 / ref_weights[idx, j],
                fmt="o",
                color=colormap[prey],
                label=prey,
            )
        ax.legend(loc="best", title="prey")
        ax.plot([vmin, vmax], [vmin, vmax], "k--", alpha=0.5)
        ax.set_xlabel(f"KSM fitted {phi_label}")
        ax.set_ylabel(f"Tree generated {phi_label}")
        ax.set_xlim(vmin, vmax)
        ax.set_ylim(vmin, vmax)

        ax = fig.add_subplot(2, 2, 4)
        self.plot_tree(ax)
        ax.set_title(f"{phi_label} RMSE = {self.get_score():.4g}")

        fig.tight_layout()

        return fig

    def to_json(self):
        return {
            "tree": self.preorder(),
            "preys": self.preys,
            "baits": self.baits,
            "poolsizes": self.poolsizes.tolist(),
            "params": self.params,
        }

    @staticmethod
    def from_json(d):
        root = AssemblyTree.decode(d["tree"])
        poolsizes = np.array(d["poolsizes"])
        return AssemblyTree(
            root=root,
            preys=d["preys"],
            baits=d["baits"],
            poolsizes=poolsizes,
            params=d["params"],
        )

    @staticmethod
    def read_json(path: str):
        with open(path, "r") as fp:
            return AssemblyTree.from_json(json.load(fp))

    def to_gdot_recursive(self, g_dot: pydot.Dot, node: Node) -> pydot.Node:
        pydot_node = pydot.Node(str(node.value))
        g_dot.add_node(pydot_node)
        if node.value < self.n_leaves:
            pydot_node.set(
                "label",
                f"{self.preys[node.value]} ({self.poolsizes[node.value]:.4f})",
            )
            pydot_node.set("shape", "octagon")
            pydot_node.set("fontcolor", "blue")
            pydot_node.set("fillcolor", "white")
        else:
            pydot_node.set(
                "label",
                f"C{node.value - self.n_leaves} "
                f"({self.poolsizes[node.value]:.4f})",
            )
            pydot_node.set("shape", "ellipse")
            pydot_node.set("fontcolor", "darkgreen")
            pydot_node.set("fillcolor", "white")
            left_pydot_node = self.to_gdot_recursive(g_dot, node.left)
            right_pydot_node = self.to_gdot_recursive(g_dot, node.right)
            edge_left = pydot.Edge(pydot_node, left_pydot_node)
            edge_right = pydot.Edge(pydot_node, right_pydot_node)
            g_dot.add_edge(edge_left)
            g_dot.add_edge(edge_right)
        return pydot_node

    def to_gdot(self) -> pydot.Dot:
        g_dot = pydot.Dot()
        pydot_root = self.to_gdot_recursive(g_dot, self.root)
        return g_dot

    def preorder(self) -> List[int]:
        """Encode the tree structure as a binary number based on pre-order."""
        return [n.value for n in self.root.preorder]

    @staticmethod
    def decode_recursive(encoding: List[int], n_leaves: int) -> Node:
        """Decode a preorder encoding back to a tree and return the root Node.

        Note that this recursive function is destructive and the 'encoding'
        argument will not be usable afterwards.
        """
        value = encoding.pop(0)
        if value < n_leaves:  # this is a leaf
            return Node(value)
        else:
            left = AssemblyTree.decode_recursive(encoding, n_leaves)
            right = AssemblyTree.decode_recursive(encoding, n_leaves)
            return Node(value, left, right)

    @staticmethod
    def decode(encoding: List[int]) -> Node:
        n_leaves = (len(encoding) + 1) / 2
        encoding_copy = list(encoding)
        return AssemblyTree.decode_recursive(encoding_copy, n_leaves)

    @staticmethod
    def generate_random_encoding(n_leaves: int) -> List[int]:
        """Generate a random binary tree."""
        # we do this by generating a random encoding, and then decoding it.
        e = [0]
        for n in range(1, n_leaves):
            i = np.random.randint(0, 2 * n - 1)
            e = e[0:i] + [n_leaves + n - 1, n] + e[i:]
        return e

    @staticmethod
    def generate_all_trees(n_leaves: int) -> List[List[int]]:
        """Iterate all unique trees that span this list of preys."""
        # if we have all the trees of size n, to generate all the ones of
        # size n+1, we simply insert a [None, n+1] in each possible location
        # in the encoding (i.e. any i = 0 .. n-1). Note that we cannot add
        # anything to the end of the encoding, since it must be a complete
        # tree and therefore terminates at the end.
        all_encodings = [[0]]
        for n in range(1, n_leaves):
            encodings_next = []
            for e in all_encodings:
                for i in range(2 * n - 1):
                    encodings_next.append(
                        e[0:i] + [n_leaves + n - 1, n] + e[i:]
                    )
            all_encodings = encodings_next
        return all_encodings

    @staticmethod
    def count_unique_trees(n: int) -> int:
        """Calculate the number of unique trees of size n.

        The formula for the number of unique trees, if we don't consider
        isomorphisms is (2n-2)! / (n-1)!

        If we filter all isomorphic duplicates, it is divided by 2^(n-1).
        This turns out to be that f(n+1)/f(n) = 2n-1

        f(2) = 1
        f(3) = f(2)*3 = 3
        f(4) = f(3)*5 = 15
        f(5) = f(4)*7 = 105
        f(6) = f(5)*9 = 945
        f(7) = f(6)*11 = 10395
        f(8) = f(7)*13 = 135135
        """
        return math.factorial(2 * n - 2) / math.factorial(n - 1) / 2 ** (n - 1)

    def get_levels(self) -> Dict[str, int]:
        """Calculate the tree-level of each prey."""
        res_dict = {}
        for level, nodes in enumerate(self.root.levels):
            for n in nodes:
                if n.height == 0:
                    res_dict[self.preys[n.value]] = level
        return res_dict

    def get_maturation_times(self) -> Dict[str, float]:
        """Calculate the total size of intermediate pools for each prey."""
        total_poolsizes = self.calc_total_pool_sizes()
        assembled_poolsize = self.poolsizes[self.root.value]
        for key in total_poolsizes.keys():
            total_poolsizes[key] -= assembled_poolsize
        return total_poolsizes

    def calculate_all_labeling(self, b: float) -> pd.DataFrame:
        data = []
        for prey in self.preys:
            for i, f in enumerate(self.calculate_prey_labeling(prey, b)):
                data.append((b, prey, i, f))
        return pd.DataFrame(data, columns=["b", "prey", "node", "labeling"])

    def calculate_prey_labeling(self, prey: str, b: float) -> np.ndarray:
        """Calculate the labeling fraction of the prey in each nodes.

        Args:
            prey: the prey (leaf) to work on
            b: the fraction of new biomass (i.e. 1 - e^{μt})

        Returns:
            labeling: an array with the labeling fractions of each node
        """
        mu_times_t = -np.log(1.0 - b)

        trajectory = self.get_trajectory_to_prey(prey)
        n = len(trajectory)
        trajectory_poolsizes = list(map(self.poolsizes.__getitem__, trajectory))

        inv_phi = [
            sum(trajectory_poolsizes[i:]) / trajectory_poolsizes[i]
            for i in range(n)
        ]
        M = np.diag(inv_phi, k=1) - np.diag([1.0 / self.lysine_psi] + inv_phi)
        f_vector = (np.eye(n + 1) - expm(M * mu_times_t)).sum(0)
        labeling = np.zeros(self.n_nodes, dtype=float) * np.nan
        for i, f in zip(trajectory, f_vector[1:]):
            # we skip f_vector[0] since it represents the lysine pool labeling
            labeling[i] = f
        return labeling

    def get_trajectory_to_prey(self, prey: str) -> List[int]:
        """Get the list of Nodes along a trajectory from leaf to root.

        Args:
            prey: the prey (leaf) to work on

        Returns:
            trajectory: list of integers (the values of the nodes)
        """
        if prey not in self.preys:
            raise KeyError(f"The prey {prey} is not on the list of preys")
        stop_value = self.preys.index(prey)
        return [
            node.value
            for node in self._get_trajectory_recursive(self.root, stop_value)[0]
        ]

    def _get_trajectory_recursive(
        self, node_start: Node, stop_value: int
    ) -> Tuple[List[Node], bool]:
        """Get the list of Nodes along a trajectory from leaf to root.

        Args:
            node: the root of the current subtree
            stop_value: the value of the node where the DFS should stop

        Returns:
            trajectory: list of Nodes
            contains_stop: a boolean flag indicating whether the node with
            the stop value is part of this subtree or not
        """
        if node_start.value == stop_value:
            return [node_start], True
        elif node_start.height > 0:
            l_trajectory, l_contains_stop = self._get_trajectory_recursive(
                node_start.left, stop_value
            )
            if l_contains_stop:
                return l_trajectory + [node_start], True

            r_trajectory, r_contains_stop = self._get_trajectory_recursive(
                node_start.right, stop_value
            )
            if r_contains_stop:
                return r_trajectory + [node_start], True

        return [], False

    def get_complexes(self) -> Dict[int, Set[str]]:
        return self._get_complexes_recursive(self.root)

    def _get_complexes_recursive(self, node: Node) -> Dict[int, Set[str]]:
        if node.height == 0:
            return {node.value: {self.preys[node.value]}}
        else:
            l_complexes = self._get_complexes_recursive(node.left)
            r_complexes = self._get_complexes_recursive(node.right)

            union_of_all_complexes = set.union(*l_complexes.values()).union(
                set.union(*r_complexes.values())
            )
            result_dict = {node.value: union_of_all_complexes}
            result_dict.update(l_complexes)
            result_dict.update(r_complexes)
            return result_dict

    def get_all_pairs(self) -> pd.DataFrame:
        # create a DataFrame of bait, prey, node for all nodes that contain
        # both bait and prey
        data = []
        for n, proteins in self.get_complexes().items():
            for bait, prey in itertools.product(proteins, proteins):
                data.append((bait, prey, n, self.poolsizes[n]))
        return pd.DataFrame(data, columns=["bait", "prey", "node", "poolsize"])

    def get_labeling_predictions(self, b_range: np.ndarray) -> pd.DataFrame:
        # create a concatenated DataFrame of all the labeling fractions of all
        # preys at all time points
        labeling_df = pd.concat(map(self.calculate_all_labeling, b_range))

        # merge with the DataFrame of all the bait-prey pairs and the nodes in
        # which they both exist (and the corresponding pool size)
        labeling_with_baits_df = labeling_df.merge(
            self.get_all_pairs(), on=["prey", "node"]
        )

        # calculate the weighted average of the labeling across all relevant
        # pools
        labeling_with_baits_df["labeling_times_poolsize"] = (
            labeling_with_baits_df.labeling * labeling_with_baits_df.poolsize
        )
        labeling_with_baits_df = (
            labeling_with_baits_df.groupby(["bait", "prey", "b"])
            .sum()[["labeling_times_poolsize", "poolsize"]]
            .reset_index()
        )
        labeling_with_baits_df["labeling"] = (
            labeling_with_baits_df.labeling_times_poolsize
            / labeling_with_baits_df.poolsize
        )

        labeling_with_baits_df.drop(
            ["labeling_times_poolsize", "poolsize"], inplace=True, axis=1
        )
        return labeling_with_baits_df
