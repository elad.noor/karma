# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os

import matplotlib.pyplot as plt
import pandas as pd
from scipy.cluster.hierarchy import dendrogram, fcluster, linkage

from . import RES_DIR


def calculate_clusters(
    data_df: pd.DataFrame, n_clusters: int, time_point: float = 30.0
):
    """Find baits to represent clusters of baits based on the raw data.

    :param data_df: The raw data (with repetitions)
    :param n_clusters: Requested number of clusters
    :param time_point: Which time point to use for the clustering
    :return:
    """

    # calculate the mean over the 3 replicates of the selected time point
    # and also keep the bait and prey
    baits = data_df.bait.unique()
    mean_data_df = data_df[data_df["Time"] == time_point]
    mean_data_df = mean_data_df.groupby(["prey", "bait"]).mean()
    mean_data_df = mean_data_df[["labeling"]].reset_index()
    mean_data_df = mean_data_df.pivot("prey", "bait", "labeling")

    # we use the clustering results to reduce the complexity of building a tree
    hierarchy = linkage(mean_data_df, method="complete", metric="correlation")
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    dendrogram(
        hierarchy,
        ax=ax,
        labels=mean_data_df.index,
    )
    fig.savefig(os.path.join(RES_DIR, "dendrogram.pdf"))

    mean_data_df["cluster"] = (
        fcluster(hierarchy, t=n_clusters, criterion="maxclust") - 1
    )

    clusters = {}
    for i in range(n_clusters):
        cluster_preys = set(mean_data_df[mean_data_df.cluster == i].index)
        preys_who_are_baits = sorted(cluster_preys.intersection(baits))
        if len(preys_who_are_baits) > 0:
            # if this cluster contains baits, we use the first one which is
            # both bait and prey as the representative
            clusters[preys_who_are_baits[0]] = cluster_preys
        else:
            # if this cluster does not contain any baits, we arbitrarily
            # use the first one as the representative
            clusters["*" + cluster_preys[0]] = cluster_preys

    return clusters
