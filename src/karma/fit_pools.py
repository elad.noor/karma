"""
Created on Fri Oct  4 14:48:33 2019

@author: noore
"""

from typing import Tuple

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from pynverse import inversefunc
from scipy.linalg import expm
from scipy.optimize import curve_fit
from tqdm import tqdm
from uncertainties import nominal_value, std_dev, ufloat, unumpy


MINIMUM_LN_PHI = -5


def labeling(b: float, ln_phi: np.ndarray) -> np.ndarray:
    """
    Calculate the expected K1 labeling for a protein
    in an exponentially growing cell with growth rate μ.

    Arguments:
        b - the fraction of new biomass (i.e. 1 - e^{μt})
        phi - the relative sizes of the pools (as a fraction of all downstream pools)
    """
    inv_phi = [np.exp(-p) for p in ln_phi.flat]
    n = len(inv_phi)

    if b == 0 or b == 1:
        return b * np.ones(n)

    mu_times_t = -np.log(1.0 - b)
    M = np.diag(inv_phi[1:], k=1) - np.diag(inv_phi)
    return (np.eye(n) - expm(M * mu_times_t)).sum(0)


def fit_phis(
    xdata: np.ndarray, ydata: np.ndarray, ln_lysine_psi: float
) -> unumpy.uarray:
    b_to_k1 = lambda b, ln_phi1, ln_phi2: labeling(
        b, np.array([ln_lysine_psi, ln_phi1, ln_phi2])
    )[2]
    timer_b_to_k1 = lambda b: b_to_k1(b, MINIMUM_LN_PHI, 0)
    timer_k1_to_b = inversefunc(timer_b_to_k1, domain=[0, 1])

    def timer_to_nup(xdata: np.ndarray, ln_phi1: float, ln_phi2: float):
        y_pred = []
        for x in xdata:
            b = timer_k1_to_b(x)
            y_pred.append(b_to_k1(b, ln_phi1, ln_phi2))
        return np.array(y_pred)

    popt1, pcov1 = curve_fit(
        timer_to_nup,
        xdata=xdata,
        ydata=ydata,
        p0=(np.log(0.5), 0),
        bounds=[(MINIMUM_LN_PHI, -1e-5), (0, 0)],
    )

    popt2, pcov2 = curve_fit(
        timer_to_nup,
        xdata=xdata,
        ydata=ydata,
        p0=(np.log(0.5), 0),
        bounds=[(MINIMUM_LN_PHI, MINIMUM_LN_PHI), (0, 0)],
    )

    # check if the model with an inaccessible pool actually improves the fit

    lsq1 = np.mean((ydata - timer_to_nup(xdata, *popt1)) ** 2)
    lsq2 = np.mean((ydata - timer_to_nup(xdata, *popt2)) ** 2)

    if lsq1 < lsq2 * (1.0 + 1e-5):
        # in cases such as bait: MLP1, prey:NUP188
        # we get absurdly large uncertainty values for phi1 and phi2
        # because of the redundancy. However, we see that phi2 is very close
        # to 1 and therefore we can use the 2-step model fit (without an
        # inaccessible pool), where the uncertainty is much smaller for phi1
        std_devs = np.diag(pcov1)
        return unumpy.uarray(popt1, std_devs=std_devs)
    else:
        std_devs = np.diag(pcov2)
        if popt2[0] < MINIMUM_LN_PHI + 1e-5:
            # in cases such as bait = "NUP84", prey = "NUP133"
            # the phi1 hits the lower bound, and its uncertainty is extremely low
            # However, this is mainly due to our choice of the bound and the
            # uncertainty might be much higher in reality
            return unumpy.uarray(popt2, std_devs=[1, std_devs[1]])
        else:
            return unumpy.uarray(popt2, std_devs=std_devs)


def fit_four_step_model(
    df: pd.DataFrame, ln_lysine_psi: float, ax=None
) -> unumpy.uarray:
    ln_phi = fit_phis(df.k1_timer.values, df.k1.values, ln_lysine_psi)

    xmin = 0
    xmax = 1

    if ax is not None:
        xrange = np.linspace(xmin, xmax, 100)
        ln_phi_nom = unumpy.nominal_values(ln_phi)

        b_to_k1 = lambda b, ln_phi1, ln_phi2: labeling(
            b, np.array([ln_lysine_psi, ln_phi1, ln_phi2])
        )[2]

        timer_k1_pred = [b_to_k1(b, MINIMUM_LN_PHI, 0) for b in xrange]
        target_k1_pred = [b_to_k1(b, *ln_phi_nom) for b in xrange]

        # to draw the curve, we need to set "b" as the hidden variable
        # and plot the NTR-timer versus the target state prediction
        ax.plot(
            timer_k1_pred, target_k1_pred, "-r", label="fit", linewidth=2, alpha=0.5
        )

        ax.scatter(x=df.k1_timer, y=df.k1, label="KARMA data")
        ax.plot([xmin, xmax], [xmin, xmax], "--k", label="NTR-timer", linewidth=0.5)
        ax.set_xlabel("NTR-timer K1 labeling", fontsize=7)
        ax.set_ylabel("target state K1 labeling", fontsize=7)
        ax.legend(fontsize=7)

    return ln_phi


def calculate_phis(
    data_df: pd.DataFrame,
    ln_lysine_psi: float,
    make_fig: bool = True,
) -> Tuple[pd.DataFrame, plt.Figure]:
    n_baits = len(data_df.bait.unique())
    n_preys = len(data_df.prey.unique())
    phi_data = []
    if make_fig:
        fig, axs = plt.subplots(
            n_preys,
            n_baits,
            figsize=(n_baits * 4, n_preys * 4),
            sharex="all",
            sharey="all",
        )
        axs_list = list(axs.flat)

    with tqdm(total=n_preys * n_baits) as pbar:
        for i, (bait, bgroup_df) in enumerate(data_df.groupby("bait")):
            pbar.set_description(f"Processing bait {bait}")
            for j, (prey, pgroup_df) in enumerate(bgroup_df.groupby("prey")):
                if make_fig:
                    ax = axs_list.pop(0)
                    ln_phi = fit_four_step_model(
                        pgroup_df, ln_lysine_psi=ln_lysine_psi, ax=ax
                    )
                    if i > 0 or j > 0:
                        ax.legend().set_visible(False)
                    ax.set_title(
                        f"bait: {bait}, prey: {prey}\n"
                        f"$\ln\phi_1$ = {ln_phi[0]}\n"
                        f"$\ln\phi_2$ = {ln_phi[1]}",
                        fontsize=8,
                    )
                    ax.set_xlabel("")
                else:
                    ln_phi = fit_four_step_model(pgroup_df, ln_lysine_psi=ln_lysine_psi)

                phi_data.append((bait, prey, ln_phi[0], ln_phi[1]))
                pbar.update(1)
    if make_fig:
        fig.tight_layout()

    phi_df = pd.DataFrame(data=phi_data, columns=["bait", "prey", "ln_phi1", "ln_phi2"])
    phi_df["ln_phi1_phi2"] = phi_df["ln_phi1"] + phi_df["ln_phi2"]
    if make_fig:
        return phi_df, fig
    else:
        return phi_df


def plot_phi_summary(phi_df, figsize=(18, 18)) -> plt.Figure:
    """draw two heatmaps for the phis."""
    fig, axs = plt.subplots(2, 3, figsize=figsize)

    vmin = min(MINIMUM_LN_PHI, MINIMUM_LN_PHI)
    for ax, label in zip(axs[0, :].flat, ["ln_phi1", "ln_phi2", "ln_phi1_phi2"]):
        pivot_df = phi_df.pivot("bait", "prey", label)
        sns.heatmap(
            pivot_df.applymap(nominal_value), ax=ax, cmap="viridis", vmin=vmin, vmax=0
        )
        ax.set_ylim(0, pivot_df.shape[0])
        ax.set_title(label + " values")

    for ax, label in zip(axs[1, :].flat, ["ln_phi1", "ln_phi2", "ln_phi1_phi2"]):
        pivot_df = phi_df.pivot("bait", "prey", label)
        sns.heatmap(pivot_df.applymap(std_dev), ax=ax, cmap="viridis", vmin=0, vmax=6)
        ax.set_ylim(0, pivot_df.shape[0])
        ax.set_title(label + " uncertainty")

    fig.tight_layout()
    return fig


def load_phis(phi_csv_path: str, remove_preys: list = None):
    """Get the phi values from a file

    :param phi_csv_path:
    :param remove_preys:
    :return:
    """
    phi_df = pd.read_csv(phi_csv_path, index_col=None)
    preys = set(phi_df.prey)
    if remove_preys:
        assert set(remove_preys).issubset(preys), (
            "Some of the preys to "
            "remove were not found: "
            f"{remove_preys} in {preys}."
        )
        preys = preys.difference(remove_preys)
    preys = sorted(preys)

    # keep only the values where both the prey and the bait are in the list
    # of possible preys (i.e. the leaves of the tree that we want to fit)
    phi_df = phi_df[phi_df.prey.isin(preys) & phi_df.bait.isin(preys)]
    baits = sorted(phi_df.bait.unique())

    phi_df["phi1"] = phi_df["Precursor state fraction"].apply(lambda x: ufloat(x, 0))
    phi_df["phi1"] += phi_df["Precursor state fraction uncertainty"].apply(
        lambda x: ufloat(0, x)
    )

    phi_df["phi2"] = phi_df["Accessible fraction of mature states"].apply(
        lambda x: ufloat(x, 0)
    )

    return phi_df, preys, baits
