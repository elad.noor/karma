import warnings
from typing import Callable, List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import rc
from matplotlib.backends.backend_pdf import PdfPages
from pynverse import inversefunc
from scipy.linalg import expm
from scipy.optimize import curve_fit
from tqdm import tqdm
from uncertainties import ufloat, unumpy


# Set plotting style
sns.set_style("ticks")
rc("font", **{"family": "sans-serif"})
rc("text", usetex=False)


def f_target(
    t: np.ndarray,
    phi: np.ndarray,
    index: int,
    growth_rate: float,
) -> np.ndarray:
    """
    Calculate the expected target pool (S2) labeling for a protein
    in an exponentially growing cell with growth rate μ.

    Arguments:
        t - time (in minutes)
        phi - the parameters of the pools (starting with the precusros pool)
        index - the index of the target pool
    """
    inv_phi = [1.0 / p for p in phi.flat]
    M = np.diag(inv_phi[1:], k=1) - np.diag(inv_phi)

    if type(t) == float:
        res = expm(M * growth_rate * t)[:, index].sum()
        return 1.0 - res  # return a float as well
    else:
        res = np.array([expm(M * x)[:, index].sum() for x in (growth_rate * t).flat])
        return 1.0 - res  # return an array with the same shape as 't'


def f_timers(t: np.ndarray, lysine_psi: float, growth_rate: float) -> np.ndarray:
    """

        internal reference proteins behave as 2-step systems with a phi = LYSINE_PSI,
        so we can use the same function for them

    Args:
        t:

    Returns:

    """
    return f_target(t, np.array([lysine_psi, 1]), index=1, growth_rate=growth_rate)


def f_timers_inverse(
    lysine_psi: float, max_time: float, growth_rate: float
) -> Callable[[np.ndarray], np.ndarray]:
    """Create the inverse function for f_target.

    Args:
        lysine_psi: the relative size of the Lysine pool

    Returns:
        A function that maps the target pool labeling to the time that we expect
        it to occur.

    """
    return inversefunc(
        lambda t: f_timers(t, lysine_psi, growth_rate),
        domain=[0, max_time],
        open_domain=False,
    )


def calculate_clustered_labeling_data(
    _df,
    clusters,
    lysine_psi: float,
    max_time: float,
    growth_rate: float,
    timer_proteins: List[str],
    baits_to_drop: List[str],
):
    timer_df = (
        _df[_df.prey.isin(timer_proteins)]
        .groupby(["bait", "time", "replicate"])
        .mean()[["k1"]]
    )
    timer_df.rename(columns={"k1": "timer_k1"}, inplace=True)
    timer_df["t"] = timer_df.timer_k1.apply(
        f_timers_inverse(
            lysine_psi=lysine_psi, max_time=max_time, growth_rate=growth_rate
        )
    )
    _df = _df[_df.kind != "NTR"]
    _df = _df[~_df.bait.isin(baits_to_drop)]

    _df["cluster"] = ""
    for k, v in clusters.items():
        _df.loc[_df.prey.isin(v), "cluster"] = k

    # drop data of preys that are in none of the clusters
    _df = _df[_df.cluster != ""]
    _df.drop("prey", axis=1, inplace=True)
    _df.rename(columns={"cluster": "prey"}, inplace=True)

    # calculate mean and stdev for each cluster-bait pair (and also across the 3
    # replicates at the same time)
    clustered_data_df = _df.groupby(["bait", "prey", "time", "replicate"]).agg(
        ["mean", "std"]
    )[["k1"]]
    clustered_data_df.columns = ["k1", "k1_std"]
    clustered_data_df = clustered_data_df.reset_index().join(
        timer_df, on=["bait", "time", "replicate"]
    )
    return clustered_data_df


def fit_phis(
    xdata: np.ndarray,
    ydata: np.ndarray,
    lysine_psi: float,
    growth_rate: float,
    min_phi: float = 1e-3,
    inaccessible: bool = True,
):
    def f_without_inaccessible(tdata: np.ndarray, phi1: float):
        return f_target(
            tdata, np.array([lysine_psi, phi1, 1]), index=2, growth_rate=growth_rate
        )

    def f_with_inaccessible(tdata: np.ndarray, phi1_phi2: float, z: float = 1.0):
        """
        when z = 0, both phi1 and phi2 are equal (phi1 = phi2 = sqrt(phi1_phi2))
        when z = 1, phi2 will be equal to 1 (phi1 = phi1_phi2)
        """
        phi1 = np.sqrt(phi1_phi2 * (1 - z + z * phi1_phi2))
        phi2 = np.sqrt(phi1_phi2 / (1 - z + z * phi1_phi2))
        return f_target(
            tdata, np.array([lysine_psi, phi1, phi2]), index=2, growth_rate=growth_rate
        )

    if inaccessible:
        popt, pcov = curve_fit(
            f_with_inaccessible,
            xdata=xdata,
            ydata=ydata,
            p0=(0.5, 0.5),
            bounds=[(min_phi, 0), (1, 1)],
        )
        mse = np.mean((ydata - f_with_inaccessible(xdata, *popt)) ** 2)
        phi1 = np.sqrt(popt[0] * (1 - popt[1] + popt[1] * popt[0]))
        phi2 = np.sqrt(popt[0] / (1 - popt[1] + popt[1] * popt[0]))
        phi1_phi2_std = np.sqrt(pcov[0, 0])
        phi1_std = phi1_phi2_std / phi2
        phi2_std = 0.0
    else:
        popt, pcov = curve_fit(
            f_without_inaccessible,
            xdata=xdata,
            ydata=ydata,
            p0=(0.5),
            bounds=[min_phi, 1],
        )
        mse = np.mean((ydata - f_without_inaccessible(xdata, *popt)) ** 2)
        phi1 = popt[0]
        phi1_std = np.sqrt(pcov[0, 0])
        phi2 = 1.0
        phi2_std = 0.0

    phi = unumpy.uarray([phi1, phi2], [phi1_std, phi2_std])
    return phi, mse


def calculate_phis(
    data_df: pd.DataFrame,
    lysine_psi: float,
    growth_rate: float,
) -> pd.DataFrame:
    n_baits = len(data_df.bait.unique())
    n_preys = len(data_df.prey.unique())
    phi_data = []

    with tqdm(total=n_preys * n_baits) as pbar:
        for bait, bgroup_df in data_df.groupby("bait"):
            pbar.set_description(f"Processing bait {bait}")
            for prey, pgroup_df in bgroup_df.groupby("prey"):
                if pgroup_df.k1.mean() > pgroup_df.timer_k1.mean():
                    # fit with an inaccessible pool, i.e. ln(phi2) lower
                    # bound is MIN_PHI
                    phi, mse = fit_phis(
                        pgroup_df.t.values,
                        pgroup_df.k1.values,
                        lysine_psi=lysine_psi,
                        growth_rate=growth_rate,
                        inaccessible=True,
                    )
                    phi_data.append((bait, prey, phi[0], phi[1], mse))
                else:
                    # fit without an inaccessible pool, i.e. ln(phi2) lower
                    # bound is 1
                    phi, mse = fit_phis(
                        pgroup_df.t.values,
                        pgroup_df.k1.values,
                        lysine_psi=lysine_psi,
                        growth_rate=growth_rate,
                        inaccessible=False,
                    )

                    phi_data.append((bait, prey, phi[0], ufloat(1, 0), mse))
                pbar.update(1)

    return pd.DataFrame(data=phi_data, columns=["bait", "prey", "phi1", "phi2", "mse"])


def plot_ksm_fits(
    data_df: pd.DataFrame,
    ksm_df: pd.DataFrame,
    bait: str,
    prey: str,
    ax: plt.Axes,
    lysine_psi: float,
    max_time: float,
    growth_rate: float,
):
    t_range = np.linspace(0, max_time, 25)

    group_data_df = data_df.loc[(data_df.bait == bait) & (data_df.prey == prey), :]
    try:
        phi1, phi2, mse = ksm_df.set_index(["bait", "prey"]).loc[(bait, prey), :]
    except KeyError:
        # warnings.warn(f"There is no data for the bait-prey pair {bait}-{prey}")
        ax.text(max_time / 2.0, 0.5, f"prey: {prey}\nNO DATA", ha="center", va="center")
        return

    phi_nom = np.array([lysine_psi, phi1.nominal_value, phi2.nominal_value])

    ax.plot(
        t_range,
        1.0 - np.exp(-t_range * growth_rate),
        color=sns.xkcd_rgb["windows blue"],
        label=r"$1 - e^{-\mu t}$",
        linewidth=1,
        zorder=1,
    )
    ax.plot(
        t_range,
        f_timers(t_range, lysine_psi=lysine_psi, growth_rate=growth_rate),
        linewidth=1,
        color=sns.xkcd_rgb["coral"],
        label="reference",
        zorder=1,
    )
    for row in group_data_df.itertuples():
        ax.plot(
            [row.t, row.t],
            [row.k1, row.timer_k1],
            linewidth=0.5,
            color=sns.xkcd_rgb["coral"],
            label=None,
            zorder=1,
        )

    ax.scatter(
        x=group_data_df.t,
        y=group_data_df.k1,
        color=sns.xkcd_rgb["amethyst"],
        label=f"prey: {prey}",
        zorder=2,
    )

    ax.plot(
        t_range,
        f_target(t_range, phi_nom, index=2, growth_rate=growth_rate),
        color=sns.xkcd_rgb["amethyst"],
        linewidth=2,
        label=f"$\phi_1$ = {phi1:.1g}, $\phi_2$ = {phi2.nominal_value:.1g}",
        zorder=2,
    )

    ax.set_xlabel("time (min)")
    ax.set_ylabel("$K_1$ labeling")
    ax.legend(fontsize=7, frameon=False)


def plot_all_ksm_fits(
    figure_fname: str,
    data_df: pd.DataFrame,
    ksm_df: pd.DataFrame,
    lysine_psi: float,
    max_time: float,
    growth_rate: float,
    n_cols: int = 4,
):
    baits = sorted(data_df.bait.unique())
    preys = sorted(data_df.prey.unique())
    n_rows = int(np.ceil(len(preys) / n_cols))

    with tqdm(total=ksm_df.shape[0]) as pbar:
        with PdfPages(figure_fname) as pdf:
            for bait in baits:
                fig, axs = plt.subplots(
                    n_rows,
                    n_cols,
                    figsize=(n_cols * 3.5, n_rows * 3.5),
                    sharex="all",
                    sharey="all",
                )
                for ax, prey in zip(axs.flat, preys):
                    pbar.set_description(f"Plotting KSM fits for {bait}-{prey}")
                    plot_ksm_fits(
                        data_df,
                        ksm_df,
                        bait,
                        prey,
                        ax=ax,
                        lysine_psi=lysine_psi,
                        growth_rate=growth_rate,
                        max_time=max_time,
                    )
                    pbar.update(1)
                fig.suptitle(f"bait: {bait}")
                pdf.savefig(fig)
                plt.close(fig)
