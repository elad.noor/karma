"""Modeling cases where a single protein is to be added to an existing tree."""
# The MIT License (MIT)
#
# Copyright (c) 2021 Department of Plant and Environmental Sciences, Weizmann
# Institute of Science, Rehovot, Israel
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from typing import Dict, Tuple
import numpy as np
from scipy.linalg import expm
import pandas as pd

from binarytree import Node

from .tree import AssemblyTree


class DynamicAssemblyTree(object):
    def __init__(
        self,
        base_tree: AssemblyTree,
        entry_node: Node,
        entry_bound_fraction: float,
        exit_node: Node,
        exit_bound_fraction: float,
    ):
        """A class for fitting a dynamic protein to an existing tree.

        Args:
            base_tree: the base tree (without the dynamic protein)
            entry_node: the node where the dynamic protein binds first
            entry_bound_fraction: the fraction of the entry node that is
            bound to the dynamic protein
            exit_node: the node from which the dynamic protein (partially)
            unbinds
            exit_bound_fraction: the fraction of dynamic protein that stays
        """
        self.base_tree = base_tree
        self.entry_node = entry_node
        self.entry_bound_fraction = entry_bound_fraction
        self.exit_node = exit_node
        self.exit_bound_fraction = exit_bound_fraction

    def calc_total_pool_sizes(self) -> dict:
        """Calculate the total pool sizes (per prey)

        Returns:
            res: a dictionary mapping preys to total pool sizes

        """
        # the dynamic bait protein doesn't affect the total pool sizes of the
        # preys
        return self.base_tree.calc_total_pool_sizes()

    def calc_precursor_pool_sizes(self) -> dict:
        """Calculate all precursor pool sizes wrt the dynamic 'bait' protein.

        Returns:
            res: a dictionary mapping preys to precursor pool sizes

        """
        # as for any other bait, the precursor pools depend on when it binds
        # first on the branch connecting the prey to the root
        res, _ = self.calc_precursor_pool_sizes_recursive(self.base_tree.root)
        return res

    def calc_precursor_pool_sizes_recursive(
        self, node: Node
    ) -> Tuple[Dict[str, float], bool]:
        """Run a DFS on tree to calculate all precursor pool sizes wrt the
        dynamic protein.

        Args:
            node: a node representing the subtree (or the root)

        Returns:
            res: a dictionary mapping preys to precursor pool sizes
            contains_bait: a boolean flag indicating whether the bait (the
            dynamic protein) is part of this subtree or not
        """
        if node == self.entry_node:
            # the dynamic protein enters right above this node,
            # so we only count the fraction that is unbound to it (i.e. α)
            alpha = self.entry_bound_fraction
            contains_bait = True
        else:
            # otherwise, we count the entire pool for the precursor pool
            alpha = 1.0
            contains_bait = False

        if node.height == 0:
            # if the current node is a leaf
            prey = self.base_tree.preys[node.value]
            return {prey: self.base_tree.poolsizes[node.value] * alpha}, contains_bait

        # recursion step: calculate the precursor pool sizes for each
        # of the two child subtrees
        res_left, contains_bait_left = self.calc_precursor_pool_sizes_recursive(
            node.left
        )
        res_right, contains_bait_right = self.calc_precursor_pool_sizes_recursive(
            node.right
        )

        # since the preys of the two subtrees are mutually exclusive,
        # sets, we can just merge the two dictionaries
        res = res_left | res_right

        # if both children don't contain the bait, add this node to all the
        # pools in the dictionary
        if not contains_bait_left and not contains_bait_right:
            for key in res.keys():
                res[key] += self.base_tree.poolsizes[node.value] * alpha
        else:
            contains_bait = True

        return res, contains_bait

    def calc_inaccessible_pool_sizes(self) -> dict:
        """Calculate all inaccessible pool sizes wrt the dynamic 'bait' protein.

        Returns:
            res: a dictionary mapping preys to inaccessible pool sizes

        """
        # the inaccessible pools depend on when it leaves the tree
        res, _ = self.calc_inaccessible_pool_sizes_recursive(self.base_tree.root)
        return res

    def calc_inaccessible_pool_sizes_recursive(
        self, node: Node
    ) -> Tuple[Dict[str, float], bool]:
        """Run a DFS on tree to calculate all inaccessible pool sizes wrt the
        dynamic protein.

        Note that we only use 'inaccessible' for lack of a better name.
        It should only refers to the fraction
        of the complexes from which the dynamic bait unbinds from, i.e. the
        result should be multiplied by (1 - exit_bound_fraction).

        Args:
            node: a node representing the subtree (or the root)

        Returns:
            res: a dictionary mapping preys to inaccessible pool sizes
            contains_bait: a boolean flag indicating whether the bait is part of
            this subtree or not
        """
        if node.height == 0:
            # if the current node is a leaf
            prey = self.base_tree.preys[node.value]
            return {prey: 0.0}, False

        # recursion step: calculate the precursor pool sizes for each
        # of the two child subtrees
        res_left, contains_bait_left = self.calc_inaccessible_pool_sizes_recursive(
            node.left
        )
        res_right, contains_bait_right = self.calc_inaccessible_pool_sizes_recursive(
            node.right
        )

        # since the preys of the two subtrees are mutually exclusive,
        # sets, we can just merge the two dictionaries
        res = res_left | res_right

        # if this is the exit node, or one of the nodes above it, add the
        # poolsize to each of the preys
        contains_bait = (
            contains_bait_left or contains_bait_right or node == self.exit_node
        )
        if contains_bait:
            for key in res.keys():
                res[key] += self.base_tree.poolsizes[node.value]

        return res, contains_bait

    def fit_ksm(self) -> float:
        """Calculate a fit score for the labeling data given this dynamic tree.

        Returns:
            score: the RMSE score between the labeling data and the predicted
            data from the Kinetic State Model based on the current tree.
        """

        pass

    def get_all_pool_sizes(self) -> pd.DataFrame:
        total = self.calc_total_pool_sizes()
        s1 = self.calc_precursor_pool_sizes()
        s3 = self.calc_inaccessible_pool_sizes()
        s2 = {p: total[p] - s1[p] - s3[p] for p in self.base_tree.preys}
        return pd.DataFrame.from_records(
            [s1, s2, s3], index=["maturation", "mature", "inaccessible"]
        ).transpose()

    def labeling(self, b: float) -> Dict[str, float]:
        """Calculate the expected labeling of the pulldowns.

        Args:
            b: the fraction of new biomass (i.e. 1 - e^{μt})
        Returns:
            f_dict: a dictionary from each prey to the expected labeling of
            the pull-down

        """
        total = self.calc_total_pool_sizes()
        s1 = self.calc_precursor_pool_sizes()
        s3 = self.calc_inaccessible_pool_sizes()
        s2 = {p: total[p] - s1[p] - s3[p] for p in self.base_tree.preys}

        if b == 0 or b == 1:
            return {prey: b for prey in self.base_tree.preys}

        f_dict = {}
        mu_times_t = -np.log(1.0 - b)
        for prey in self.base_tree.preys:
            if s2[prey] <= 0:
                # in this case, we have only a 2-state KSM
                inv_phi = [total[prey] / s1[prey], 1.0]
                M = np.diag(inv_phi[1:], k=1) - np.diag(inv_phi)
                f_vector = (np.eye(M.shape[0]) - expm(M * mu_times_t)).sum(0)

                # since there is no mature pool, the labeling is equal to the
                # labeling of the inaccessible pool
                f = f_vector[1]
            else:
                inv_phi = [
                    total[prey] / s1[prey],
                    1.0 + s3[prey] / s2[prey],
                    1.0,
                ]
                M = np.diag(inv_phi[1:], k=1) - np.diag(inv_phi)
                f_vector = (np.eye(M.shape[0]) - expm(M * mu_times_t)).sum(0)

                # the labeling of the pull-down is equal to a weighted sum of
                # the labeling of the mature and inaccessible pool (based on
                # their sizes and the exit_bound_fraction
                f = (
                    f_vector[1] * s2[prey]
                    + f_vector[2] * s3[prey] * self.exit_bound_fraction
                ) / (s2[prey] + s3[prey] * self.exit_bound_fraction)

            f_dict[prey] = f
        return f_dict
