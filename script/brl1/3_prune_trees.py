# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import gzip
import json

from karma import RES_DIR


PERCENT_TO_KEEP = 0.1

parser = argparse.ArgumentParser(description="Filter only the top scoring trees.")
parser.add_argument(
    "infile", metavar="INFILE", help="the input file (compressed JSON)", type=str
)
parser.add_argument(
    "-p",
    "--precent_to_keep",
    help=r"percent of top scoring trees to keep (default = 0.1%)",
    type=float,
    default=PERCENT_TO_KEEP,
)
args = parser.parse_args()

TREE_GZIP_FNAME = RES_DIR / (args.infile + ".json.gz")

with gzip.open(TREE_GZIP_FNAME, "r") as fp:
    res_json = json.load(fp)

n_trees = int(len(res_json["trees"]) * args.precent_to_keep / 100.0)

# replace the full list of trees, with only the top scoring n_trees
ordered_trees = sorted(res_json["trees"], key=lambda x: x["score"])
res_json["all_scores"] = [x["score"] for x in ordered_trees]
res_json["trees"] = ordered_trees[0:n_trees]

OUTPUT_FNAME = RES_DIR / (args.infile + f"_top_{n_trees}.json")
with open(OUTPUT_FNAME, "w") as fp:
    json.dump(res_json, fp, indent=2)
