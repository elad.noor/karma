# Is Brl1 a chaperone?
# In this notebook, we use the 10-handle KARMA labeling data and combine it
# with data for Brl1 (which was one of the original 10 handles).
# We try to find assembly trees that fit the data, where Brl1 is a unique
# protein in the sense that it binds and unbinds from the complex at
# two different points.

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from uncertainties import nominal_value, std_dev

from karma import DATA_DIR, RES_DIR
from karma.ksm import (
    calculate_clustered_labeling_data,
    calculate_phis,
    plot_all_ksm_fits,
)


NEW_RES_DIR = RES_DIR / "brl1"
if not os.path.exists(NEW_RES_DIR):
    os.mkdir(NEW_RES_DIR)

KARMA_10_HANDLE_DATA_FILE = os.path.join(
    DATA_DIR, "2020-02-01_10Handles_ZZ_K1_NUP_NTR_NTR-like_labeling.tsv"
)
KARMA_BRL1_DATA_FILES = [
    os.path.join(DATA_DIR, "2021-07-28_BRL1_Timecourse_NUPsNTRs_metabolicLabeling.tsv"),
    os.path.join(DATA_DIR, "2021-08-30_BRL1Labeling_10HandlePD.tsv"),
]

# we use the result from the previous KARMA assays, asusming that the lysine
# pool size hasn't changed
LYSINE_PSI = 0.35
GROWTH_RATE = 0.0078  # 1/min
MAX_TIME = 150

# use the following proteins as the reference labeling proteins (i.e.
# 100% dynamic proteins that have no precursor pool).
TIMER_PROTEINS = ["KAP95", "MEX67", "KAP123"]

# drop the following baits
BAITS_TO_DROP = ["NSP1", "GLE1", "NDC1"]

# define the new list of preys by clustering them
# the key will be the new name for the cluster and the values
# are the list of handles in the cluster.
# the * before the cluster name indicates that we do not have info
# about it as a bait.
CLUSTERS_WITH_BRL1 = {
    "MLP1": ["MLP1"],
    "*MLP2": ["MLP2"],
    "NUP1": ["NUP1"],
    "NUP57": ["NUP49", "NUP57"],
    "*NUP53": ["NUP53", "NUP170", "POM152", "POM34"],
    "NUP82": ["NUP82", "NUP159"],
    "NUP84": ["NUP84", "NUP133", "NUP145C"],
    "NUP85": ["NUP85", "NUP120"],
    "NUP188": ["NUP188"],
    "BRL1": ["BRL1"],
}

# drop NUP159 as a bait because Brl1 was not detected in their pulldowns
BAITS_TO_DROP += ["NUP159"]

# drop the NUP82 cluster (as a prey) because both NUP82 and NUP159 were too
# noisy in the Brl1 pulldown
# CLUSTERS_WITH_BRL1.pop("NUP82")

# drop the MLP2 cluster since it is the least informative anyway and we want
# to reduce the runtime.
CLUSTERS_WITH_BRL1.pop("*MLP2")

#%% Step 1: fitting a KSM model to all NUPs (including Brl1)

karma_10_handle_df = pd.read_csv(KARMA_10_HANDLE_DATA_FILE, delimiter="\t")
karma_combined_df = pd.concat(
    [karma_10_handle_df]
    + [pd.read_csv(fname, delimiter="\t") for fname in KARMA_BRL1_DATA_FILES]
)
clustered_data_df = calculate_clustered_labeling_data(
    karma_combined_df,
    clusters=CLUSTERS_WITH_BRL1,
    lysine_psi=LYSINE_PSI,
    max_time=MAX_TIME,
    growth_rate=GROWTH_RATE,
    timer_proteins=TIMER_PROTEINS,
    baits_to_drop=BAITS_TO_DROP,
)

# Run the KSM fitting and plot the results
clustered_ksm_df = calculate_phis(
    clustered_data_df, lysine_psi=LYSINE_PSI, growth_rate=GROWTH_RATE
)
#clustered_ksm_df.fillna()


figure_fname = NEW_RES_DIR / "clusters_ksm_fits_with_Brl1.pdf"
plot_all_ksm_fits(
    figure_fname,
    clustered_data_df,
    clustered_ksm_df,
    n_cols=3,
    lysine_psi=LYSINE_PSI,
    max_time=MAX_TIME,
    growth_rate=GROWTH_RATE,
)

_df = clustered_ksm_df.copy()

DOUBLING_TIME = np.log(2) / GROWTH_RATE
maturation_time = DOUBLING_TIME * _df.phi1 / (1.0 - _df.phi1)
maturation_rate = GROWTH_RATE * (1.0 / _df.phi1 - 1.0)

_df["Precursor state fraction"] = _df.phi1.apply(nominal_value)
_df["Precursor state fraction uncertainty"] = _df.phi1.apply(std_dev)
_df["Accessible fraction of mature states"] = _df.phi2.apply(nominal_value)
_df["Maturation time [min]"] = maturation_time.apply(nominal_value)
_df["Maturation time [min] uncertainty"] = maturation_time.apply(std_dev)
_df["Maturation rate [1/min]"] = maturation_rate.apply(nominal_value)
_df["Maturation rate [1/min] uncertainty"] = maturation_rate.apply(std_dev)

_df.drop(["phi1", "phi2", "mse"], axis=1, inplace=True)
_df.round(4).to_csv(NEW_RES_DIR / "clusters_ksm_fits_with_Brl1.csv", index=False)

# plot a heatmap of all fits
fig, axs = plt.subplots(1, 2, figsize=(10, 5))
vmin = 0
for ax, col in zip(
    axs.flat, ["Precursor state fraction", "Accessible fraction of mature states"]
):
    pivot_df = _df.pivot("bait", "prey", col)
    sns.heatmap(pivot_df, ax=ax, cmap="viridis", vmin=0, vmax=1)
    ax.set_ylim(0, pivot_df.shape[0])
    ax.set_title(col)

fig.tight_layout()
fig.savefig(NEW_RES_DIR / "clusters_ksm_heatmap_with_Brl1.svg")
