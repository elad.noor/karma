import json

import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd
from tqdm import tqdm

from karma import RES_DIR
from karma.fit_pools import load_phis
from karma.tree import AssemblyTree
NEW_RES_DIR = RES_DIR / "brl1"

# tree fitting results with Brl1 and NUP82 (without MLP2), psi = 0.35
TREE_JSON_FNAME = NEW_RES_DIR / "all_trees_2021-09-09_11:53:06_top_2027.json"

# loading results from parallel run
with open(TREE_JSON_FNAME, "r") as fp:
    res_json = json.load(fp)

phi_df, preys, baits = load_phis(NEW_RES_DIR / "clusters_ksm_fits_with_Brl1.csv")

level_stats = []
maturation_time_stats = []
for tree_json in res_json["trees"]:
    tree = AssemblyTree.from_json(tree_json["assembly_tree"])
    tree.set_reference_phis(phi_df)
    level_stats.append(tree.get_levels())
    maturation_time_stats.append(tree.get_maturation_times())

level_df = pd.DataFrame(data=level_stats)
mtime_df = pd.DataFrame(data=maturation_time_stats)


rc('font',**{'family': 'sans-serif'})
rc('text', usetex=False)

colormap = {"NUP85": "#bd42ff",
            "NUP84": "#ef0052",
            "NUP145C": "#ef0052",
            "NUP133": "#ef0052",
            "*NUP53": "#ff9400",
            "NUP82": "#5aa518",
            #"NUP159": "#5aa518",
            "NUP57": "#5aa5c6",
            "NUP49": "#5aa5c6",
            "NUP188": "#4200ff",
            "MLP1": "#bdbdbd",
            #"*MLP2": "#ff8080",
            "NUP1": "#a99480",
            "BRL1": "#ff8080",
            }

# Generate a single PDF file with all 0.1% top trees (in batches of 40=8x5 per page)
with PdfPages(NEW_RES_DIR / "top_permille_graphs.pdf") as pdf:

    fig, ax = plt.subplots(1, 1, figsize=(7, 3.5))
    hist_range = (0, len(preys)+1)
    number_of_bins = len(preys)+1
    binned_data_sets = []

    prey_order = level_df.mean().sort_values(ascending=False).index.tolist()

    for prey in prey_order:
        levels = level_df[prey]
        binned_data_sets.append(np.histogram(levels, range=hist_range, bins=number_of_bins)[0])

    binned_maximums = np.max(binned_data_sets, axis=1)
    x_locations = np.arange(0, len(binned_data_sets)) * np.max(binned_maximums)

    # The bin_edges are the same for all of the histograms
    bin_edges = np.linspace(hist_range[0], hist_range[1], number_of_bins + 1)
    centers = 0.5 * (bin_edges + np.roll(bin_edges, 1))[:-1]
    heights = np.diff(bin_edges)

    for x_loc, binned_data, prey in zip(x_locations, binned_data_sets, prey_order):
        lefts = x_loc - 0.5 * binned_data
        ax.barh(0.5+centers, binned_data, height=heights, left=lefts, color=colormap[prey],
                linewidth=1, edgecolor="k")
        ax.plot(x_loc, level_df[prey].mean(), 'kx')

    ax.set_xticks(x_locations)
    ax.set_xticklabels(prey_order)
    ax.set_yticks(range(1, 10))
    ax.set_title("Top 0.1% of trees")
    ax.set_ylabel("Level of prey in the tree")
    ax.set_xlim(-1000, None)

    fig.savefig(NEW_RES_DIR / "tree_level_histogram.pdf")
    pdf.savefig(fig)
    plt.close(fig)

    # Plot histogram of all top scores
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    all_scores = [tree_json["score"] for tree_json in res_json["trees"]]
    ax.hist(all_scores, bins=50)
    ax.set_title(f"Top {len(all_scores)} of trees")
    ax.set_xlabel("Score (RMSE)")
    ax.set_ylabel("Number of trees")

    fig.savefig(NEW_RES_DIR / "tree_score_histogram.pdf")
    pdf.savefig(fig)
    plt.close(fig)

    # Plot graphs for all the top trees
    n_trees = 3#len(res_json["trees"])
    n_cols = 5
    n_rows = 8
    with tqdm(total=n_trees) as pbar:
        for i in np.arange(0, n_trees, n_cols*n_rows):
            fig, axs = plt.subplots(n_rows, n_cols, figsize=(n_cols*6, n_rows*6))

            for ax, tree_json in zip(axs.flat, res_json["trees"][i:]):
                tree = AssemblyTree.from_json(tree_json["assembly_tree"])
                tree.plot_tree(ax=ax)
                ax.set_title(f"score = {tree_json['score']:.6f}")
                pbar.update(1)
            pdf.savefig(fig)
            plt.close(fig)