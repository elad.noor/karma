# The MIT License (MIT)
#
# Copyright (c) 2019 Institute for Molecular Systems Biology, ETH Zurich.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import argparse
import gzip
import json
import os.path
from datetime import datetime

import numpy as np
from p_tqdm import p_map, t_map
from scipy.optimize import minimize

from karma import RES_DIR, AssemblyTree
from karma.fit_pools import load_phis


parser = argparse.ArgumentParser(
    description="A script that iterate all "
    "possible trees, and uses "
    "gradient descent to fit each "
    "ones poolsizes."
)
parser.add_argument(
    "infile", help="path to the .CSV file with the phi values", type=str
)
parser.add_argument(
    "-j", "--num_cpus", help="number of CPUs to use", default=4, type=int
)
parser.add_argument(
    "-i", "--iters", help="number of random iterations per " "tree", default=1, type=int
)
parser.add_argument(
    "-f",
    "--fixed_pools",
    help="use fixed pool sizes rather than optimize them for " "each tree",
    action="store_true",
)
parser.add_argument(
    "-r",
    "--random_samples",
    help="number of random trees to sample. The default value ("
    "0) indicates a full exhaustive screen of all trees",
    default=0,
    type=int,
)
parser.add_argument(
    "--remove_preys",
    nargs="+",
    help="Remove the following preys from the tree search",
    required=False,
)
parser.add_argument("--test_mode", help="for debugging purposes", action="store_true")
args = parser.parse_args()

phi_df, preys, baits = load_phis(args.infile, remove_preys=args.remove_preys)
n_leaves = len(preys)
poolsizes = np.ones(n_leaves * 2 - 1)


def get_score(encoding, iterations=args.iters):
    root = AssemblyTree.decode(encoding)
    tree = AssemblyTree(root, preys, baits, poolsizes, params=args.__dict__)
    tree.set_reference_phis(phi_df)
    best_score = np.inf
    best = None

    n_pools = tree.poolsizes.shape[0]

    for i in range(iterations):
        x0 = np.random.rand(n_pools)
        res = minimize(
            lambda x: tree.update_and_get_score(x),
            x0=x0,
            bounds=[(1e-5, 1.0)] * n_pools,
        )
        if res.fun < best_score:
            best = tree.to_json()
            best_score = res.fun

    return {"assembly_tree": best, "score": best_score}


def get_fixed_score(encoding):
    """Calculate score by assuming all poolsizes are equal (no optimization).

    :param encoding:
    :return:
    """
    root = AssemblyTree.decode(encoding)
    tree = AssemblyTree(
        root, preys, baits, poolsizes, allow_inaccessible=args.allow_inaccessible
    )
    tree.set_reference_phis(phi_df)
    return {
        "assembly_tree": tree.to_json(),
        "score": tree.get_score(corr=args.corr, phi_mode=args.phi_mode),
    }


if args.random_samples > 0:
    encodings = [
        AssemblyTree.generate_random_encoding(n_leaves)
        for _ in range(args.random_samples)
    ]
    prefix = "random_trees"
else:
    encodings = AssemblyTree.generate_all_trees(n_leaves)
    prefix = "all_trees"

if args.test_mode:
    results = t_map(get_fixed_score if args.fixed_pools else get_score, encodings[0:5])
else:
    results = p_map(
        get_fixed_score if args.fixed_pools else get_score,
        encodings,
        num_cpus=args.num_cpus,
    )

isotime = datetime.now().isoformat("_", timespec="seconds")
fname = os.path.join(RES_DIR, f"{prefix}_{isotime}.json.gz")

with gzip.open(fname, "wt") as fp:
    json.dump(
        {"args": args.__dict__, "phis": phi_df.to_json(), "trees": results},
        fp,
        indent=2,
    )
