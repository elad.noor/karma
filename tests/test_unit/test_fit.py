import itertools

import numpy as np
import pandas as pd
import pytest
from scipy.optimize import minimize
from uncertainties import ufloat

from karma import AssemblyTree


def test_simple_fit(iterations=1):
    n_leaves = 8
    encodings = AssemblyTree.generate_all_trees(n_leaves)

    ref_encoding = encodings[0]
    ref_poolsizes = np.random.rand(n_leaves * 2 - 1)
    ref_poolsizes /= ref_poolsizes.max()

    preys = [f"P{i}" for i in range(n_leaves)]
    baits = [f"P{i}" for i in range(n_leaves)]
    atree = AssemblyTree(
        AssemblyTree.decode(ref_encoding), preys, baits, ref_poolsizes, params={}
    )
    atree.update_phis()
    phis = atree.phis

    phi_df = (
        pd.DataFrame(
            data=phis,
            columns=preys,
            index=baits,
        )
        .reset_index()
        .melt(id_vars="index")
    )
    phi_df.columns = ["bait", "prey", "phi1"]
    phi_df["phi1"] = phi_df["phi1"].apply(lambda x: ufloat(x, 0.001))
    n_leaves = len(preys)

    best_encoding = None
    best_encoding_score = np.inf

    for encoding in encodings:
        root = AssemblyTree.decode(encodings[0])
        poolsizes = np.ones(n_leaves * 2 - 1)

        params = {}
        tree = AssemblyTree(root, preys, baits, poolsizes, params=params)
        tree.set_reference_phis(phi_df)

        best_score = np.inf
        best = None
        n_pools = tree.poolsizes.shape[0]
        for i in range(iterations):

            # x0 = np.random.rand(n_pools)
            x0 = ref_poolsizes
            res = minimize(
                lambda x: tree.update_and_get_score(x),
                x0=x0,
                bounds=[(1e-5, 3.0)] * n_pools,
            )
            if res.fun < best_score:
                best = tree.to_json()
                best_score = np.log(res.fun)

        assert best_score == pytest.approx(-3, abs=1e-3)
