# KARMA

Kinetic Analysis of incorporation Rates in Macromolecular Assemblies.
This code was used to generate the results published in:

Onischenko, Evgeny, Elad Noor, Jonas S. Fischer, Ludovic Gillet, Matthias Wojtynek, Pascal Vallotton, and Karsten Weis.
[**Maturation Kinetics of a Multiprotein Complex Revealed by Metabolic Labeling.**](https://doi.org/10.1016/j.cell.2020.11.001)
Cell 183, no. 7 (December 23, 2020): 1785-1800.e26.


## Installation
In your shell, run the following commands:
```sh
git clone https://gitlab.com/elad.noor/karma.git
cd karma
python -m venv venv
source venv/bin/activate
pip install -U pip setuptools wheel
pip install -e .
deactivate
```

## Usage

In order to launch the Jupyter notebook:
```sh
source venv/bin/activate
jupyter lab
```
the notebook will then open in your browser and you will be able to run
the code that generates the figures used in the publication.


If you would like to rerun the exhaustive tree fitting analysis (which takes
about 200 hours of CPU time, but can be parallelized):
```sh
source venv/bin/activate
python -m script.1_fit_poolsizes -j <NUM_CPUS>
```
The results will be written to the `res` folder.
