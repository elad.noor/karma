Data file descriptions
======================

* `2021-07-28_KARMA10Handle_NUPsNTRs_metabolicLabeling.tsv` - 10-handle labeling data for NUPs and NTR reference proteins
* `2021-07-28_BRL1_Timecourse_NUPsNTRs_metabolicLabeling.tsv` - Brl1 labeling data for NUPs and NTR reference proteins

